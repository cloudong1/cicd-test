<?php
// MySQL 서버에 연결
$host = 'localhost'; // 호스트 이름
$user = 'root'; // 사용자 이름
$password = '0000'; // 비밀번호
$database = 'database_name'; // 데이터베이스 이름

// MySQL 서버에 연결
$conn = mysqli_connect($host, $user, $password, $database);

// 연결 확인
if (!$conn) {
    die("MySQL 연결 실패: " . mysqli_connect_error());
} else {
    echo "MySQL 연결 성공!";
}

// 연결 종료
mysqli_close($conn);
?>